import { ReactNode } from "react";
import clsx from "clsx";

export default function Button(props: {
    variant?: "primary" | "secondary" | "default";
    href?: string;
    className?: string;
    children?: ReactNode;
    // TODO fix this code it doesnt have to be here it could be HTMLElement right?
    type?: "button" | "submit" | "reset";
    onClick?: (...args: any) => any;
}) {
    // eslint-disable-next-line prefer-const
    let { variant, href, className, children, type, ...rest } = props;
    variant = variant || "default";


    const classes = clsx(
        className,
        "unset cursor-pointer py-2 px-3 transition " +
        "font-semibold rounded-md shadow border " +
        "focus-visible:outline focus-visible:outline-2 " +
        "focus-visible:outline-offset-2 focus-visible:outline-indigo-600",
        variant === "primary" && "bg-indigo-600 border-indigo-500 text-slate-200 hover:bg-indigo-500",
        variant === "secondary" && "bg-indigo-600 border-indigo-500 text-slate-200 hover:bg-indigo-500",
        variant === "default" && "bg-slate-700 border-slate-600 text-slate-200 hover:bg-slate-600",
    );

    if (href) return (
        <a href={href} className={classes} {...rest}>
            {children}
        </a>
    );

    return (
        <button className={classes} type={type} {...rest}>
            {children}
        </button>
    );
}
