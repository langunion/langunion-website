"use client";
import Navbar, { NavbarItem } from "@/ui/Navbar";
import {
    ChatBubbleLeftRightIcon,
    GlobeAsiaAustraliaIcon,
    AcademicCapIcon,
} from "@heroicons/react/24/outline"
import {
    UserIcon,
    RectangleGroupIcon,
    EnvelopeIcon
} from "@heroicons/react/20/solid"
import { IconBuildingCommunity, IconWriting, IconMessageCircleQuestion } from "@tabler/icons-react";

export default function Navigation() {
    const items: NavbarItem[] = [
        {
            label: "Apps",
            dropdown: [
                {
                    label: "babbeln",
                    description: "chat with people",
                    href: "/apps/babbeln",
                    icon: ChatBubbleLeftRightIcon,
                },
                {
                    label: "buffeln",
                    description: "learn vocabulary",
                    href: "/apps/buffeln",
                    icon: GlobeAsiaAustraliaIcon,
                },
                {
                    label: "school",
                    description: "learn grammer",
                    href: "/apps/school",
                    icon: AcademicCapIcon,
                },
            ],
            callToActions: [
                { label: "Create an account", href: "/login", icon: UserIcon },
                { label: "Contact us", href: "/contact", icon: EnvelopeIcon },
                { label: "View all apps", href: "/apps", icon: RectangleGroupIcon },
            ],
            fullwidth: true,
        },
        {
            label: "Blog",
            href: "/blog",
        },
        {
            label: "Company",
            dropdown: [
                {
                    label: "About us",
                    description: "about us",
                    href: "/about-us",
                    icon: IconBuildingCommunity,
                },
                {
                    label: "Blog",
                    description: "blog",
                    href: "/blog",
                    icon: IconWriting,
                },
                {
                    label: "FAQs",
                    description: "faqs",
                    href: "/faq",
                    icon: IconMessageCircleQuestion,
                },
            ],
            callToActions: [
                { label: "Create an account", href: "/login", icon: UserIcon },
                { label: "Contact us", href: "/contact", icon: EnvelopeIcon },
            ],
            fullwidth: false,
        }
    ];

    return (
        <Navbar items={items} />
    );
}
