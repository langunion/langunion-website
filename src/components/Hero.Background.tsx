export default function HeroBackground() {
    return (
        <div>
            {/* Light circle top right */}
            <div
                className="pointer-events-none absolute -inset-px -z-10 rounded-xl"
                style={{
                    background: `
                        radial-gradient(
                            650px circle at calc(100% - 100px) 100px,
                            rgba(14, 165, 233, 0.15),
                            transparent 80%
                        )`,
                }}
            ></div>
        <svg
            aria-hidden="true"
            className="absolute inset-0 -z-10 h-full w-full stroke-slate-600
                         [mask-image:radial-gradient(100%_100%_at_top_right,black,transparent)]"
        >
            <defs>
                <pattern
                    x="50%"
                    y={-1}
                    id="83fd4e5a-9d52-42fc-97b6-718e5d7ee527"
                    width={200}
                    height={200}
                    patternUnits="userSpaceOnUse"
                >
                    <path d="M100 200V.5M.5 .5H200" fill="none" />
                </pattern>
            </defs>
            <rect fill="url(#83fd4e5a-9d52-42fc-97b6-718e5d7ee527)" width="100%" height="100%" strokeWidth={0} />
        </svg>
        </div>
    );
}
