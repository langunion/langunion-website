"use client"
import HeroBackground from "@/components/Hero.Background";
import HeroPhone from "@/components/Hero.Phone";
import Button from "@/components/Button";

export default function Hero() {
    return (
        <div className="group relative isolate pt-14">
            <HeroBackground />
            <div className="mx-auto max-w-7xl px-6 py-24 sm:py-32 lg:flex lg:items-center lg:gap-x-10 lg:px-8 lg:py-40">
                <div className="mx-auto max-w-2xl lg:mx-0 lg:flex-auto">
                    <h1 className="mt-10 max-w-lg text-4xl font-bold tracking-tight text-slate-200 sm:text-6xl">
                        The modern way to learn languages
                    </h1>
                    <p className="mt-6 text-lg leading-8 text-slate-300">
                        Langunion has everything you need to learn any language for free.
                        Want to learn grammar, vocabulary or speak with natives of your target language?
                        We have you covered!
                    </p>
                    <div className="mt-10 flex items-center gap-6 flex-wrap">
                        <Button href="/login" variant="primary">Get started</Button>
                        <Button href="/apps">Learn more <span aria-hidden="true">→</span></Button>
                    </div>
                </div>

                <div className="mt-16 sm:mt-24 lg:mt-0 lg:flex-shrink-0 lg:flex-grow">
                    <HeroPhone />
                </div>
            </div>
        </div>
    );
}
