import { XMarkIcon } from "@heroicons/react/20/solid"
import Link from "next/link";

export default function Banner() {
    return (
        <div
            className="flex items-center gap-x-6 bg-indigo-700
                       border-b border-indigo-600 px-6 py-2.5 sm:px-3.5 sm:before:flex-1"
        >
            <p className="text-sm text-white">
                <Link href="/login">
                    All <strong>premium</strong> features
                    for <strong>free</strong> right now!
                    <svg viewBox="0 0 2 2" aria-hidden="true" className="mx-2 inline h-0.5 w-0.5 fill-current">
                        <circle r={1} cx={1} cy={1} />
                    </svg>
                    <strong>Register now&nbsp;<span aria-hidden="true">&rarr;</span></strong>
                </Link>
            </p>
            <div className="flex flex-1 justify-end">
                <button type="button" className="-m-3 p-3 focus-visible:outline-offset-[-4px]">
                    <span className="sr-only">Dismiss</span>
                    <XMarkIcon aria-hidden="true" className="h-5 w-5 text-white" />
                </button>
            </div>
        </div>
    )
}
