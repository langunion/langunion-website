"use client";
import { ReactNode, MouseEvent } from "react";
import { motion, useMotionTemplate, useMotionValue } from "framer-motion";
import Button from "@/components/Button";

export default function ShowcaseApp(props: {
    name: string;
    description: ReactNode;
    href: string;
    screenshot: string;
    color?: "blue" | "purple" | "pink";
}) {
    const mouseX = useMotionValue(0);
    const mouseY = useMotionValue(0);

    function handleMouseMove({
        currentTarget,
        clientX,
        clientY,
    }: MouseEvent) {
        const { left, top } = currentTarget.getBoundingClientRect();

        mouseX.set(clientX - left);
        mouseY.set(clientY - top);
    }

    function getColor(): string {
        const color = props.color || "blue";
        switch (color) {
            case "blue":
                return "rgba(14, 165, 233, 0.15)";
            case "purple":
                return "rgba(146, 21, 211, 0.15)";
            case "pink":
                return "rgba(255, 52, 171, 0.15)";
        }
    };

    return (
        <div className="py-12">
            <div className="mx-auto max-w-7xl sm:px-6 lg:px-8">
                <div
                    className="group relative isolate overflow-hidden
                               bg-slate-800 border border-slate-700 px-6 py-20
                               sm:rounded-3xl"
                    onMouseMove={handleMouseMove}
                >
                    <motion.div
                        className="-z-20 pointer-events-none absolute -inset-px
                                   rounded-xl opacity-0 transition duration-300
                                   group-hover:opacity-100"
                        style={{
                            background: useMotionTemplate`
                            radial-gradient(
                            650px circle at ${mouseX}px ${mouseY}px,
                            ${getColor()},
                            transparent 80%
                            )
                        `,
                        }}
                    />
                    <div className="mx-auto grid max-w-2xl grid-cols-1
                                    gap-x-8 gap-y-16 sm:gap-y-20 lg:mx-0 lg:max-w-none lg:grid-cols-2
                                    lg:items-center lg:gap-y-0">
                        <div className="flex flex-col gap-4 justify-center items-start">
                            <header className="flex flex-col gap-2 pb-4">
                                <h2 className="text-3xl font-bold tracking-tight text-white sm:text-4xl">
                                    {props.name}
                                </h2>

                                <div className="rounded-lg h-[0.2rem] bg-slate-500/50" />
                            </header>

                            <p className="text-base leading-8 text-slate-400">
                                {props.description}
                            </p>
                            <Button href={props.href}>Download</Button>
                        </div>
                        <img
                            alt="Product screenshot"
                            src="https://tailwindui.com/img/component-images/dark-project-app-screenshot.png"
                            width={2432}
                            height={1442}
                            className="relative -z-20 min-w-full max-w-xl rounded-xl
                                 shadow-xl ring-1 ring-white/10 lg:row-span-4 lg:w-[38rem] lg:max-w-none"
                        />
                    </div>
                </div>
            </div>
        </div>
    )
}
