"use client";
import { useState } from "react";
import Link from "next/link";
import Button from "@/components/Button";
import { Field, Label, Input, Checkbox } from "@headlessui/react"
import { CheckIcon } from '@heroicons/react/16/solid';
import { IconBrandGitlab } from "@tabler/icons-react";

export default function Footer() {
    const [agreed, setAgreed] = useState(false)

    const socials = [
        {
            name: "GitHub",
            href: "https://gitlab.com/langunion",
            icon: <IconBrandGitlab aria-hidden="true" className="h-6 w-6" />,
        },
    ];

    const linkColumns = [
        {
            title: "About us",
            links: [
                {
                    name: "About us",
                    href: "/about-us",
                },
                {
                    name: "FAQs",
                    href: "/faq",
                },
                {
                    name: "Blog",
                    href: "/blog",
                },
                {
                    name: "Contact us",
                    href: "/contact",
                },
            ]
        },
        {
            title: "Others",
            links: [
                {
                    name: "License",
                    href: "/license",
                },
                {
                    name: "Code",
                    href: "https://gitlab.com/langunion",
                    newTab: true,
                }               
            ]
        },
    ];

    return(
        <div className="pt-10">
            <footer className="bg-slate-800 text-slate-300 text-sm p-8 overflow-hidden">
                <div className="flex flex-wrap gap-14 max-w-[1200px] m-auto items-start justify-between">
                    <div className="flex flex-row gap-6 flex-wrap">
                        <header className="flex flex-col gap-2">
                            <Link
                                href="/"
                                className="flex gap-2 items-center"
                            >
                                <div className="w-[2rem] h-[2rem] bg-slate-700 border border-slate-600 rounded-lg"></div>
                                <h1 className="font-bold">langunion</h1>
                            </Link>

                            <p>T.: +49 (0) 1523 4794964</p>
                            <p>E.: info@langunion.com</p>

                            <nav aria-label="Footer" className="flex gap-2">
                                {socials.map((social, key) => (
                                    <a key={key} href={social.href} className="text-gray-400 hover:text-white transition">
                                        <span className="sr-only">{social.name}</span>
                                        {social.icon}
                                    </a>
                                ))}
                            </nav>
                        </header>

                        {linkColumns.map((column, key) => (
                            <aside key={key}>
                                <h1 className="font-bold pb-4">{column.title}</h1>
                                <ul className="flex flex-col gap-2">
                                    {column.links.map((item, key) => (
                                        <li key={key} className="hover:text-white transition">
                                            {!item.newTab && (
                                                <Link href={item.href}>
                                                    {item.name}
                                                </Link>
                                            )}
                                            {item.newTab && (
                                                <a href={item.href} target="_blank" rel="noopener noreferrer">
                                                    {item.name}
                                                </a>
                                            )}
                                        </li>  
                                    ))}
                                </ul>
                            </aside>
                        ))}
                    </div>

                    <aside className="flex flex-col gap-4">
                        <h2 className="text-center text-lg font-bold text-white">
                            Subscribe to our newsletter.
                        </h2>

                        <form className="flex flex-col gap-4">
                            <Field className="flex flex-wrap gap-4 items-center">
                                    <Checkbox
                                        checked={agreed}
                                        onChange={setAgreed}
                                        className="group size-6 rounded-md bg-white/10 p-1 cursor-pointer
                                                ring-1 ring-white/20 ring-inset
                                                data-[checked]:bg-white transition"
                                    >
                                        <CheckIcon className="hidden size-4 fill-black group-data-[checked]:block" />
                                    </Checkbox>
                                    <Label className="text-sm text-slate-400">
                                        By selecting this, you agree to our{" "}
                                        <Link href="/privacy-policy" className="font-semibold text-indigo-100">
                                            privacy&nbsp;policy
                                        </Link>
                                        .
                                    </Label>
                            </Field>

                            <Field className="flex flex-wrap gap-4">
                                <Input
                                    id="email-address"
                                    name="email"
                                    type="email"
                                    required
                                    placeholder="Enter your email"
                                    autoComplete="email"
                                    className="min-w-0 flex-auto rounded-md border-0
                                            bg-white/5 px-3.5 py-2 text-white shadow-sm
                                            ring-1 ring-inset ring-white/20 focus:ring-2
                                            focus:ring-inset focus:ring-white/50"
                                />
                                <Button type="submit">Notify me</Button>
                            </Field>
                        </form>
                    </aside>
                </div>
            </footer>
        </div>
    )
}
