import Button from "@/components/Button";

export default function NotFound() {
    return (
        <main className="mx-auto flex w-full max-w-7xl flex-auto flex-col justify-center px-6 py-24 sm:py-64 lg:px-8">
            <p className="text-base font-semibold leading-8 text-indigo-500">
                404
            </p>
            <h1 className="mt-4 text-3xl font-bold tracking-tight sm:text-5xl">
                Page not found
            </h1>
            <p className="mt-6 text-base leading-7 text-slate-500">
                Sorry, the page you&apos;ve visited does not exist or is currently under construction.
            </p>
            <div className="mt-10">
                <Button href="/">
                    <span aria-hidden="true">&larr;</span> Back to home
                </Button>
            </div>
        </main>
    );
}
