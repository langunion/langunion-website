import { Fragment } from "react";
import Hero from "@/components/Hero";
import ShowcaseApp from "@/components/ShowcaseApp";

export default function Home() {
    const apps = [
        {
            name: "babbeln",
            description: <Fragment>
                {"Chat with people from everywhere."}
                <br />
                {"Speak with natives of your target language or"}
                {"learn with teachers from everywhere, wherever you live."}
            </Fragment>,
            href: "/apps/babbeln",
            screenshot: "https://placehold.co/1200x720",
            color: "blue" as const,
        },
        {
            name: "buffeln",
            description: <Fragment>
                {"Chat with people from everywhere."}
                <br />
                {"Speak with natives of your target language or"}
                {"learn with teachers from everywhere, wherever you live."}
            </Fragment>,
            href: "/apps/buffeln",
            screenshot: "https://placehold.co/1200x720",
            color: "purple" as const,
        },
        {
            name: "school",
            description: <Fragment>
                {"Chat with people from everywhere."}
                <br />
                {"Speak with natives of your target language or"}
                {"learn with teachers from everywhere, wherever you live."}
            </Fragment>,
            href: "/apps/school",
            screenshot: "https://placehold.co/1200x720",
            color: "pink" as const,
        },
    ];

    return (
        <main>
            <Hero />
            {apps.map((app, key) => <ShowcaseApp key={key} {...app} />)}
        </main>
    );
}
