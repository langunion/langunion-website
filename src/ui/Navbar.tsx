"use client";
import { useState } from "react"
import Link from "next/link";
import {
    Dialog,
    DialogPanel,
    Disclosure,
    DisclosureButton,
    DisclosurePanel,
    Popover,
    PopoverButton,
    PopoverGroup,
    PopoverPanel,
} from "@headlessui/react"
import {
    Bars3Icon,
    SquaresPlusIcon,
    XMarkIcon,
} from "@heroicons/react/24/outline"
import { ChevronDownIcon } from "@heroicons/react/20/solid"
import Button from "@/components/Button";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type ReactFC = any;

export type NavbarItem = {
    label: string;
    href?: string;
    dropdown?: Array<{
        label: string;
        description: string;
        href: string;
        icon: ReactFC;
    }>;
    callToActions?: Array<{
        label: string;
        href: string;
        icon: ReactFC;
    }>;
    fullwidth?: boolean;
};

export default function Navigation(props: {
    items: NavbarItem[];
}) {
    const [mobileMenuOpen, setMobileMenuOpen] = useState(false);

    return (
        <header className="relative isolate z-10 bg-slate-900 border-b border-slate-600 shadow-sm sticky top-0">
            <nav aria-label="Global" className="mx-auto flex max-w-7xl items-center justify-between p-4 px-8">
                {/*TODO left item nav aka. logo */}
                <Link href="/" className="group flex lg:flex-1 gap-2 items-center">
                    <div className="h-10 w-10 bg-slate-700 border border-slate-600 rounded-lg" />
                    <h1 className="font-bold group-hover:text-white transition max-[250px]:hidden">
                        langunion
                    </h1>
                </Link>

                <div className="flex lg:hidden">
                    <Button
                        onClick={() => setMobileMenuOpen(true)}
                        className="group text-slate-400
                              bg-slate-800 border border-slate-700
                              hover:bg-slate-700 hover:text-slate-300 transition
                              flex gap-2"
                    >
                        <span className="max-[300px]:hidden">Menu</span>
                        <Bars3Icon
                            aria-hidden="true"
                            className="h-6 w-6 text-slate-500 group-hover:text-slate-400"
                        />
                    </Button>
                </div>

                <PopoverGroup className="hidden lg:flex lg:gap-x-12">
                    {props.items.map((item, idx) => {
                        const hasDropdown: boolean = !!item.dropdown?.length && !!item.callToActions?.length;
                        if (hasDropdown && item.fullwidth) {
                            return (
                                <Popover key={idx}>
                                    <PopoverButton
                                        className="flex items-center gap-x-1 font-semibold hover:text-white transition"
                                    >
                                        {item.label}
                                        <ChevronDownIcon aria-hidden="true" className="h-5 w-5 flex-none text-slate-400" />
                                    </PopoverButton>

                                    <PopoverPanel
                                        transition
                                        className="absolute inset-x-0 top-0 -z-10 bg-slate-900 pt-14
                                                   shadow-lg transition
                                                   data-[closed]:-translate-y-1 data-[closed]:opacity-0
                                                   data-[enter]:duration-200 data-[leave]:duration-150
                                                   data-[enter]:ease-out data-[leave]:ease-in"
                                    >
                                        <div className="mx-auto grid max-w-7xl grid-cols-4 gap-x-4 px-6 py-10 lg:px-8 xl:gap-x-8">
                                            {item.dropdown!.map((dropdownItem, idx) => (
                                                <div
                                                    key={idx}
                                                    className="group relative rounded-lg p-4
                                                        border border-transparent
                                                        hover:border hover:border-slate-600
                                                        hover:bg-slate-800 transition"
                                                >
                                                    <div className="flex h-11 w-11 items-center justify-center
                                                        rounded-lg bg-slate-600
                                                        transition border border-slate-500"
                                                    >
                                                        <dropdownItem.icon
                                                            aria-hidden="true"
                                                            className="h-6 w-6 text-slate-400 group-hover:text-white transitions"
                                                        />
                                                    </div>
                                                    <a href={dropdownItem.href} className="mt-6 block font-semibold">
                                                        {dropdownItem.label}
                                                        <span className="absolute inset-0" />
                                                    </a>
                                                    <p className="mt-1 text-sm text-slate-400">{dropdownItem.description}</p>
                                                </div>
                                            ))}

                                            {/* TODO last item in dropdown and grid cols */}
                                            <div className="group relative rounded-lg p-6
                                                bg-slate-800 border border-slate-600
                                                hover:bg-slate-700 transition">
                                                <div className="flex h-11 w-11 items-center justify-center rounded-lg
                                                    bg-slate-600 border border-slate-500"
                                                >
                                                    <SquaresPlusIcon
                                                        aria-hidden="true"
                                                        className="h-6 w-6 text-white"
                                                    />
                                                </div>
                                                <a href="/apps" className="mt-6 block font-semibold text-white">
                                                    view all apps
                                                    <span className="absolute inset-0" />
                                                </a>
                                                <p className="mt-1 text-sm text-slate-40g">see all the apps we have to offer</p>
                                            </div>
                                        </div>

                                        <div className="bg-slate-700 border-y border-slate-500">
                                            <div className="mx-auto max-w-7xl px-6 lg:px-8">
                                                <div className="grid grid-cols-3 divide-x divide-slate-500 border-x border-slate-500">
                                                    {item.callToActions!.map((item, idx) => (
                                                        <a
                                                            key={idx}
                                                            href={item.href}
                                                            className="group flex items-center justify-center gap-x-2.5 text-sm
                                                                p-3 font-semibold text-slate-300
                                                                hover:bg-slate-600 hover:text-white transition"
                                                        >
                                                            <item.icon
                                                                aria-hidden="true"
                                                                className="h-5 w-5 flex-none text-slate-300
                                                                 group-hover:text-white transition"
                                                            />
                                                            {item.label}
                                                        </a>
                                                    ))}
                                                </div>
                                            </div>
                                        </div>
                                    </PopoverPanel>
                                </Popover>
                            );
                        } else if (hasDropdown && !item.fullwidth) {
                            return (
                                <Popover key={idx} className="relative">
                                    <PopoverButton
                                        className="flex items-center gap-x-1 font-semibold hover:text-white transition"
                                    >
                                        {item.label}
                                        <ChevronDownIcon aria-hidden="true" className="h-5 w-5 flex-none text-slate-400" />
                                    </PopoverButton>

                                    <PopoverPanel
                                        transition
                                        className="absolute -left-8 top-full z-10 mt-3 w-screen max-w-md overflow-hidden
                                                   rounded-3xl bg-slate-800 border border-slate-700 shadow-lg
                                                   transition data-[closed]:translate-y-1 data-[closed]:opacity-0
                                                   data-[enter]:duration-200 data-[leave]:duration-150 data-[enter]:ease-out
                                                   data-[leave]:ease-in"
                                    >
                                        <div className="p-4">
                                            {item.dropdown!.map((item, idx) => (
                                                <div
                                                    key={idx}
                                                    className="group relative flex items-center gap-x-6
                                                        rounded-lg p-4 border border-transparent
                                                        hover:bg-slate-700 hover:border-slate-600 transition"
                                                >
                                                    <div
                                                        className="flex h-11 w-11 flex-none items-center
                                                                   justify-center rounded-lg bg-slate-600
                                                                   border border-slate-500"
                                                    >
                                                        <item.icon
                                                            aria-hidden="true"
                                                            className="h-6 w-6 text-slate-400
                                                                         group-hover:text-white transition"
                                                        />
                                                    </div>
                                                    <div className="flex-auto">
                                                        <Link href={item.href} className="block font-semibold">
                                                            {item.label}
                                                            <span className="absolute inset-0" />
                                                        </Link>
                                                        <p className="mt-1 text-sm text-slate-400">{item.description}</p>
                                                    </div>
                                                </div>
                                            ))}
                                        </div>
                                        <div className="grid grid-cols-2 divide-x divide-slate-500 bg-slate-700">
                                            {item.callToActions!.map((item, idx) => (
                                                <Link
                                                    key={idx}
                                                    href={item.href}
                                                    className="group flex items-center justify-center gap-x-2.5 p-3
                                                        text-sm font-semibold text-slate-300
                                                        hover:bg-slate-600 hover:text-white transition"
                                                >
                                                    <item.icon
                                                        aria-hidden="true"
                                                        className="h-5 w-5 flex-none group-hover:text-white transition"
                                                    />
                                                    {item.label}
                                                </Link>
                                            ))}
                                        </div>
                                    </PopoverPanel>
                                </Popover>
                            );                         
                        } else {
                            return (
                                <Link
                                    key={idx}
                                    href={item.href || "#"}
                                    className="font-semibold hover:text-white transition"
                                >
                                    {item.label}
                                </Link>
                            );
                        }
                    })}
                </PopoverGroup>

                {/*TODO right item nav */}
                <div className="hidden lg:flex lg:flex-1 lg:justify-end">
                    <Button href="/login" variant="primary">
                        Log in <span aria-hidden="true">&rarr;</span>
                    </Button>
                </div>
            </nav>

            <Dialog
                open={mobileMenuOpen}
                onClose={setMobileMenuOpen}
                className="lg:hidden"
            >
                <DialogPanel
                    transition
                    className="fixed top-0 left-0 z-10
                               w-full p-4 px-8
                               bg-slate-800/90 backdrop-blur-lg
                               border border-slate-600 p-4 rounded-md
                               duration-200 ease-out data-[closed]:opacity-0
                               "
                >
                    <div className="flex items-center justify-between">
                        <a href="#" className="-m-1.5 p-1.5">
                            <span className="sr-only">langunion</span>
                            <div
                                className="h-10 w-10 bg-slate-700 border border-slate-600 rounded-lg"
                            />
                        </a>
                        <button
                            type="button"
                            onClick={() => setMobileMenuOpen(false)}
                            className="-m-2.5 rounded-md p-2.5 text-gray-700"
                        >
                            <span className="sr-only">Close menu</span>
                            <XMarkIcon
                                aria-hidden="true"
                                className="h-6 w-6 text-slate-400 hover:text-white transition"
                            />
                        </button>
                    </div>

                    <div className="mt-6 flow-root">
                        <div className="-my-6 divide-y divide-slate-600">
                            <div className="space-y-2 py-6">
                                {props.items.map((item, idx) => {
                                    const hasDropdown: boolean = !!item.dropdown?.length && !!item.callToActions?.length;
                                    if (hasDropdown && item.fullwidth) {
                                        return (
                                            <Disclosure key={idx} as="div" className="-mx-3">
                                                <DisclosureButton
                                                    className="group flex w-full items-center justify-between
                                                   rounded-lg py-2 pl-3 pr-3.5 font-semibold
                                                   border border-transparent
                                                   hover:bg-slate-700 hover:border-slate-600
                                                   transition"
                                                >
                                                    {item.label}
                                                    <ChevronDownIcon
                                                        aria-hidden="true"
                                                        className="h-5 w-5 flex-none group-data-[open]:rotate-180"
                                                    />
                                                </DisclosureButton>
                                                <DisclosurePanel className="mt-2 space-y-2">
                                                    {[...item.dropdown!, ...item.callToActions!].map((item, idx) => (
                                                        <DisclosureButton
                                                            key={idx}
                                                            as="a"
                                                            href={item.href}
                                                            className="block rounded-lg py-2 pl-6 pr- font-semibold
                                                                border border-transparent
                                                                hover:bg-slate-700 hover:border-slate-600
                                                                transition"
                                                        >
                                                            {item.label}
                                                        </DisclosureButton>
                                                    ))}
                                                </DisclosurePanel>
                                            </Disclosure>

                                        );
                                    } else if (hasDropdown && !item.fullwidth) {
                                        return (
                                            <Link
                                                key={idx}
                                                href={item.href || "#"}
                                                className="-mx-3 block rounded-lg px-3 py-2 font-semibold
                                                     border border-transparent hover:bg-slate-700 hover:border-slate-600
                                                     transition"
                                            >
                                                {item.label}
                                            </Link>
                                        );
                                    } else {
                                        return (
                                            <Link
                                                key={idx}
                                                href={item.href || "#"}
                                                className="-mx-3 block rounded-lg px-3 py-2 font-semibold
                                                     border border-transparent hover:bg-slate-700 hover:border-slate-600
                                                     transition"
                                            >
                                                {item.label}
                                            </Link>
                                        );
                                    }
                                })}
                            </div>
                            <div className="py-6">
                                <Link
                                    href="/login"
                                    className="-mx-3 block rounded-lg px-3 py-2 font-semibold
                                          border border-transparent hover:bg-slate-700 hover:border-slate-600
                                          transition"
                                >
                                    Log in
                                </Link>
                            </div>
                        </div>
                    </div>
                </DialogPanel>
            </Dialog>
        </header>
    )
}
